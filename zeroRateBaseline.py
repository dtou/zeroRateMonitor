import os
import numpy as np
import re
import operator
from RunDBAPI import rundbapi
from RunDBAPI.rundbapi import RequestError
from ROOT import TFile, TH1, TDirectoryFile, TH2, TCanvas, TH1F

saveset_dir = "/hist/Savesets/2017/LHCb2/Moore2Saver" # the top level folder to analyse for a baseline
hlt2_histname_re = re.compile("Hlt2\\w+\\s*Lines")
minimum_lumi = 1e7 

class zeroRateBaseline:
	def __init__(self):
		self.__lumi = []
		self.__empty_lines_count = []
		self.__line_empty_rate = {}

	def append_run(self, filename):
		file = TFile.Open(filename)
		if not (self.check_rootfile_exists(file)):
			return

		try:
			run = int(filename.split('-')[1])
		except (IndexError, ValueError):
			return

		self.append_info(run, file)

	def check_rootfile_exists(self, file):
		try:
			TFile.IsZombie(file)
			return True
		except ReferenceError:
			return False

	def append_info(self, run, file):
		def should_append(lumi, run_info):
			lumi_logic = bool(lumi >= minimum_lumi)
			run_logic = (run_info['LHCState'] == 'PHYSICS')
			run_logic = (run_info['destination'] == 'OFFLINE') and run_logic
			run_logic = (run_info['veloPosition'] == 'Closed') and run_logic
			return (lumi_logic and run_logic)

		#check if run info can be obtained
		try:
			run_info = rundbapi.get_run_info(run)
		except RequestError:
			return

		#check if runinfo has the information we want
		try:
			run_info['avLumi']
			run_info['lumiTrigger']
		except KeyError:
			return

		#check if rootfile has our histogram
		try:
			folder = file.Get("Hlt2OnlineGlobalMonitor")
			lumi_hist = folder.Get("Hlt2Lumi Lines")
			lumi_hist.GetBinContent(1)
		except AttributeError:
			return

		lumi = self.get_lumi(run_info, file)
		if (should_append(lumi, run_info)):
			self.__lumi.append(lumi)
			self.__empty_lines_count.append(self.count_empty_lines(file))

	def get_lumi(self, run_info, file):
		def __get_run_info(run_info):
			lumi_trigger_rate = float(run_info['lumiTrigger'])
			avg_lumi = float(run_info['avLumi'])
			return lumi_trigger_rate, avg_lumi

		def __get_processed_lumi_events(rootfile):
			folder = rootfile.Get("Hlt2OnlineGlobalMonitor")
			lumi_hist = folder.Get("Hlt2Lumi Lines")
			lumi_events = lumi_hist.GetBinContent(1)
			return lumi_events

		def __calculate_processed_lumi(lumi_events, lumi_rate, avg_lumi):
			processed_runtime = lumi_events/lumi_rate
			processed_lumi = processed_runtime*avg_lumi
			return processed_lumi

		lumi_rate, avg_lumi = __get_run_info(run_info)
		lumi_events = __get_processed_lumi_events(file)
		processed_lumi = __calculate_processed_lumi(lumi_events, lumi_rate, avg_lumi)
		return processed_lumi

	def count_empty_lines(self, file):
		def __is_empty_line(hist, line_index):
			line_events = hist.GetBinContent(line_index)
			return line_events == 0

		def __record_line(hist, line_index):
			line_name = hist.GetXaxis().GetBinLabel(line_index)
			try:
				self.__line_empty_rate[line_name] += 1
			except KeyError:
				self.__line_empty_rate[line_name] = 1

		def __check_for_empty_lines(hist):
			for line_index in range(1, hist.GetNbinsX()):
				if(__is_empty_line(hist, line_index)):
					__record_line(hist, line_index)
					self.__empty_lines += 1

		def __check_single_histogram(hist_name, folder):
			if (hlt2_histname_re.match(hist_name)):
				hist = folder.Get(hist_name)
				__check_for_empty_lines(hist)

		def __check_hlt2_histograms(folder):
			for key in folder.GetListOfKeys():
				hist_name = key.GetName()
				__check_single_histogram(hist_name, folder)

		self.__empty_lines = 0
		folder = file.Get("Hlt2OnlineGlobalMonitor")
		__check_hlt2_histograms(folder)
		return self.__empty_lines

	def get_info(self):
		return self.__lumi, self.__empty_lines_count, self.__line_empty_rate

class zeroRateLinesPlotter:
	def __init__(self, empty_lines_dict):
		self.__empty_lines_dict = empty_lines_dict
		self.__canvas = TCanvas("c", "c", 800, 600)
		self.__bin_iterator = 1
		self.__plot_counter = 1

	def plot(self, bins_per_plot):
		self.__plot = TH1F("plot", "Number of runs with zero rate", bins_per_plot, 0, 1)
		sorted_lines = self.sort_by_count()
		for (line_name, count) in sorted_lines:
			self.plot_single_line(line_name, count)
			if (self.should_print(bins_per_plot)):
				self.print_plot()
				self.reset_plot()

	def sort_by_count(self):
		sorted_lines = sorted(self.__empty_lines_dict.items(), key=operator.itemgetter(1))
		return sorted_lines

	def plot_single_line(self, line_name, count):
		self.__plot.SetBinContent(self.__bin_iterator, count)
		self.__plot.GetXaxis().SetBinLabel(self.__bin_iterator, line_name)
		self.__bin_iterator += 1

	def should_print(self, bins_per_plot):
		return self.__bin_iterator == (bins_per_plot + 1)

	def print_plot(self):
		self.__plot.SetMinimum(0)
		self.__plot.Draw()
		self.__canvas.SaveAs("zero_rate_lines-" + str(self.__plot_counter) + ".pdf")
		self.__plot_counter += 1

	def reset_plot(self):
		self.__canvas.Clear()
		self.__plot.Reset()
		self.__bin_iterator = 1

def get_savesets():
	savesets = []
	for d, _, fs in os.walk(saveset_dir):
		for f in fs:
			savesets.append(os.path.join(d, f))
	return savesets

def get_baseline_info(savesets):
	baselineAnalyser = zeroRateBaseline()
	for saveset in savesets:
		baselineAnalyser.append_run(saveset)
	luminosity, empty_lines, line_empty_count = baselineAnalyser.get_info()
	return luminosity, empty_lines, line_empty_count

def save_baseline(luminosity, empty_lines):
	luminosity = np.asarray(luminosity)
	empty_lines = np.asarray(empty_lines)
	np.savez("zeroRates.npz", lumi=luminosity, count=empty_lines)

def plot_empty_lines(line_empty_count):
	plotter = zeroRateLinesPlotter (line_empty_count)
	plotter.plot(10)

def make_baseline_plot(savesets):
	luminosity, empty_lines, line_empty_count = get_baseline_info(savesets)
	plot_empty_lines(line_empty_count)
	save_baseline(luminosity, empty_lines)

def main():
	savesets = get_savesets()
	make_baseline_plot(savesets)

main()