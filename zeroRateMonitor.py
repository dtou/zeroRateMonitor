import re
import os
import socket
import time
from ROOT import TFile, TH1, TDirectoryFile
from RunDBAPI import rundbapi
from RunDBAPI.rundbapi import RequestError
from Monitoring.Analyser import Analyser
from Configurables import MonitoringJob


hlt2_histname_re = re.compile("Hlt2\\w+\\s*Lines")
lumi_threshold = 1e7 # A documentation of this threshold can be found at https://gitlab.cern.ch/dtou/zeroRateMonitor

class ZeroRateAnalyser(Analyser):
	def __init__(self, name, **kwargs):
		super(ZeroRateAnalyser, self).__init__(name, **kwargs)
		self.__zero_rate_lines = []

	def analyse(self, app_name, filename):
		self.Info('analyse %s %s' % (app_name, filename))
		run = 0
		file = TFile.Open(filename)

		if not (self.check_rootfile_exists(file)):
			return

		try:
			run = int(filename.split('-')[1])
		except (IndexError, ValueError):
			return

		if self.should_check_run(file, run):
			self.check_zero_rate(file)
			self.flag_zero_rate(run)
		else:
			return

	def check_rootfile_exists(self, file):
		try:
			TFile.IsZombie(file)
			return True
		except ReferenceError:
			return False


	def should_check_run(self, file, run):

		def __check_run_state(run_info):
			run_state_logic = (run_info['LHCState'] == 'PHYSICS')
			run_state_logic = (run_info['destination'] == 'OFFLINE') and run_state_logic
			run_state_logic = (run_info['veloPosition'] == 'Closed') and run_state_logic
			return run_state_logic

		def __get_run_info(run_info):
			lumi_trigger_rate = float(run_info['lumiTrigger'])
			avg_lumi = float(run_info['avLumi'])
			return lumi_trigger_rate, avg_lumi

		def __get_processed_lumi_events(file):
			folder = file.Get("Hlt2OnlineGlobalMonitor")
			lumi_hist = folder.Get("Hlt2Lumi Lines")
			lumi_events = lumi_hist.GetBinContent(1)
			return lumi_events

		def __calculate_processed_lumi(lumi_events, lumi_rate, avg_lumi):
			processed_runtime = lumi_events/lumi_rate
			processed_lumi = processed_runtime*avg_lumi
			return processed_lumi

		#check if run info can be obtained
		try:
			run_info = rundbapi.get_run_info(run)
		except RequestError:
			return False

		if __check_run_state(run_info):
			lumi_events = __get_processed_lumi_events(file)
			lumi_rate, avg_lumi = __get_run_info(run, run_info)
			processed_lumi = __calculate_processed_lumi(lumi_events, lumi_rate, avg_lumi)
			return bool(processed_lumi > lumi_threshold)
		else:
			return False

	def check_zero_rate(self, saveset_file):
		def __is_empty_line(hist, line_index):
			line_events = hist.GetBinContent(line_index)
			return line_events == 0

		def __record_zero_rate(hist, line_index):
			line_name = hist.GetXaxis().GetBinLabel(line_index)
			self.__zero_rate_lines.append(line_name)

		def __check_for_empty_lines(hist):
			for line_index in range(1, hist.GetNbinsX()):
				if(__is_empty_line(hist, line_index)):
					__record_zero_rate(hist, line_index)

		def __check_single_histogram(hist_name, folder):
			if (hlt2_histname_re.match(hist_name)):
				hist = folder.Get(hist_name)
				__check_for_empty_lines(hist)

		def __check_hlt2_histograms(folder):
			for key in folder.GetListOfKeys():
				hist_name = key.GetName()
				__check_single_histogram(hist_name, folder)

		folder = saveset_file.Get("Hlt2OnlineGlobalMonitor")
		__check_hlt2_histograms(folder)

	def flag_zero_rate(self, runID):

		def __has_zero_rate_lines():
			length = len(self.__zero_rate_lines)
			return length != 0

		def __report_zero_rate(run):
			message = "Run %d has lines with zero rate:\n" %run
			for line_name in self.__zero_rate_lines:
				message += "\t%s\n" %line_name
			self.Warning(message)

		def __clear_line_list():
			del self.__zero_rate_lines[:]

		if (__has_zero_rate_lines()):
			__report_zero_rate(runID)
			__clear_line_list()


def run(outputLevel=3, **kwargs):

	mj = MonitoringJob(**kwargs)
	mj.JobName = "ZeroRateMonitor"

	from GaudiPython.Bindings import AppMgr
	gaudi = AppMgr()

	reg_con = mj.getProp('RegistrarConnection')
	analyser = ZeroRateAnalyser('ZeroRateAnalyser',
								Applications=['Moore2Saver'],
								RegistrarConnection=reg_con,
								HistogramDirectory=mj.JobName,
								OutputLevel=mj.getProp("OutputLevel"),
								RunInMain=False)

	from Monitoring.MonitoringJob import initialize, start
	initialize(gaudi)
	try:
		_, monSvc = start()
	except RuntimeError, e:
		print e
		return

	while True:
		time.sleep(1)


if __name__ == '__main__':
    run()