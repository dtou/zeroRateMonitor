# About this repo
Documents the study for the zero rate monitor, mainly a brief analysis of empty lines and how the threshold is determined.

## Files
1. [zeroRateMonitor.py](https://gitlab.cern.ch/dtou/zeroRateMonitor/blob/master/zeroRateMonitor.py)
  * The python code found in Online/Monitoring
2. [zeroRateBaseline.py](https://gitlab.cern.ch/dtou/zeroRateMonitor/blob/master/zeroRateBaseline.py)
  * Used to analyse zero rate lines in 2017 Moore2Saver savesets
3. [plotBaseline.py](https://gitlab.cern.ch/dtou/zeroRateMonitor/blob/master/plotBaseline.py)
  * Plots the output from zeroRateBaseline.py

## Usage
First, log into any of the plus nodes of LHCb Online. Then, setup with lb-run Online:
```sh
lb-run --ext chardet --ext idna Online/latest bash --norc
```

Now run [zeroRateBaseline.py](https://gitlab.cern.ch/dtou/zeroRateMonitor/blob/master/zeroRateBaseline.py):
```sh
python zeroRateBaseline.py
```

This will run over all the savesets produced during LHCb's 2017 run. The `minimum_lumi` variable thresholds the luminosity when counting the number of zero rate runs for individual HLT2 lines. This script will produce two things:
1. Plots of lines with zero rate
  * For each HLT2 line, plot the number of runs it has zero rate in 2017 - if there is at least one run with zero rate.
2. .npz file of counts and processed luminosty
  * For each run, record the number of zero rate lines and processed luminosty inside NumPy arrays.

Now that we have run-by-run luminosity and empty lines count, [plotBaseline.py](https://gitlab.cern.ch/dtou/zeroRateMonitor/blob/master/plotBaseline.py) will plot the information to choose our threshold:
```sh
python plotBaseline.py
```

This will produce a plot:[threshold.pdf](https://gitlab.cern.ch/dtou/zeroRateMonitor/blob/master/threshold.pdf) where straight line shows the threshold of `1e7` chosen for the zeroRateMonitor.