import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

npzfile = np.load("zeroRates.npz")
count = npzfile['count']
lumi = npzfile['lumi']

range = np.array([[0, 1e8],[0, 500]])
hist, xbin, ybin = np.histogram2d(lumi, count, bins=100, range=range)

# H needs to be rotated and flipped
hist = np.rot90(hist)
hist = np.flipud(hist)
 
# Mask zeros
maskedHist = np.ma.masked_where(hist==0,hist) # Mask pixels with a value of zero
 
# Plot 2D histogram using pcolor
fig2 = plt.figure()
plt.pcolormesh(xbin, ybin, maskedHist)
plt.xlabel('Processed Luminosity')
plt.ylabel('Number of Empty Lines')
cbar = plt.colorbar()
cbar.ax.set_ylabel('Number of Runs')

#Add a straight line to show the luminosity threshold
ax = plt.gca()    
l = mlines.Line2D([1e7, 1e7], [0, 500])
ax.add_line(l)

plt.savefig('test.pdf')

